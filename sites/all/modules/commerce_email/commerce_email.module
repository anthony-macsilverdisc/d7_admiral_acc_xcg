<?php

/**
 * @file
 * Defines additional menu item and order html email functonality.
 */

/**
 * Implements hook_menu().
 */
function commerce_email_menu() {
  $items = array();

  $items['admin/commerce/config/email'] = array(
    'title' => 'Emails',
    'description' => 'Administer commerce emails',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('variable_group_form', 'commerce_email'),
    'access arguments' => array('configure store'),
    'type' => MENU_NORMAL_ITEM,
    'weight' => 99,
  );

  return $items;
}

/**
 * Implements hook_theme()
 */
function commerce_email_theme() {
  return array(
    'commerce_email_order_items' => array(
      'variables' => array('commerce_order_wrapper' => NULL),
    ),
  );
}

/**
 * Theme function for the commerce order.
 */
function theme_commerce_email_order_items($variables) {
  $table = commerce_email_prepare_table($variables['commerce_order_wrapper']);
  return theme('table', $table);
}

/**
 * Implements hook_form_FORM_ID_alter()
 */
function commerce_email_form_variable_group_form_alter(&$form, &$form_state) {
  if ($form_state['build_info']['args'][0] == 'commerce_email') {
    $form['tokens'] = array(
      '#markup' => theme('token_tree', array('token_types' => array('commerce-order', 'commerce-customer-profile'), '#global_types' => FALSE)),
      '#weight' => 99999
    );
  }
}

/**
 * Returns a rendered email of the commerce order
 * or an array of the table details
 *
 * @param $order
 *   The commerce order object
 *   
 * @param $theme
 * (optional) Defaults to TRUE
 *   Flag to return the contents of the order
 *   as a themed html table or an array suitable for theme('table' ...)
 *   
 * @return
 *   String containing the rendered order table
 *   or an array suitable for theme('table' ...)
 */
function commerce_email_order_items($order, $theme = TRUE) {
  $wrapper = entity_metadata_wrapper('commerce_order', $order);

  if ($theme) {
    return theme('commerce_email_order_items', array('commerce_order_wrapper' => $wrapper));
  }
  else {
    return commerce_email_prepare_table($wrapper);
  }
}

/**
 * Returns the labels and values of any attributes
 * selected on the product.
 *
 * @param $product_id
 *   A commerce product id
 *
 * @return
 *   String containing the attributes
 */
function commerce_email_order_item_attributes($product_id) {
  $product = commerce_product_load($product_id);
  $product_wrapper = entity_metadata_wrapper('commerce_product', $product);

  $instances = field_info_instances('commerce_product', $product->type);
  $attr = array();
  foreach ($instances as $name => $instance) {
    $commerce_cart_settings = commerce_cart_field_instance_attribute_settings($instance);
    if ($commerce_cart_settings['attribute_field'] == 1) {
      $field = field_info_field($instance['field_name']);
      if (!empty($product_wrapper->$field['field_name']->value()->name)) {
        $attr[] = htmlentities($instance['label'] . ': ' . $product_wrapper->$field['field_name']->value()->name, ENT_QUOTES, "UTF-8");
      }
    }
  }

  $title_attr = '';
  if (!empty($attr)) {
    $title_attr .= '<br /><em>' . join("<br />", $attr) . '</em>';
  }

  return $title_attr;
}

/**
 * Returns a table header and rows for theming.
 *
 * @param $wrapper
 *   A metadata-wrapped commerce_order entity
 *
 * @return array
 *   Array suitable for use by theme('table' ...)
 */
function commerce_email_prepare_table($wrapper) {
  $currency_code = $wrapper->commerce_order_total->currency_code->value();
  $amount = number_format(commerce_currency_amount_to_decimal($wrapper->commerce_order_total->amount->value(), $currency_code), 2);
  
  /*
  $header = array(
    array('data' => t('Product'), 'style' => array('text-align: left;')),
    array('data' => t('Qty'), 'style' => array('text-align: left;')),
    array('data' => t('Price (@currency_code)', array('@currency_code' => $currency_code)), 'style' => array('text-align: left;'))
  );
  */
  $header = array(
    array('data' => '', 'style' => array('display:none;'))
  );
  
  $rows = array();
  foreach ($wrapper->commerce_line_items as $delta => $line_item_wrapper) {
    // $line_item_wrapper->getBundle(); == $line_item_wrapper->type->value()
    switch ($line_item_wrapper->type->value()) {

      case 'ajw_product_attributes_line_item_custom':
        $custom_item = views_embed_view('line_items_custom', 'default', $line_item_wrapper->getIdentifier()); 
        if($custom_item) {
            $rows[] = array(
                'data' => array(
                    array('data' => $custom_item)
                )
            );
        }
        break;
      
      case 'product':
        // Sample cards are different line item type and they don't have field_custom_cards field
        // so will use view to display them
        $sample_item = views_embed_view('line_items_sample_products', 'default', $line_item_wrapper->getIdentifier());
        if($sample_item) {
            $rows[] = array(
                'data' => array(
                    array('data' => $sample_item),
                )
            );
        }
        break;
    
      case 'shipping':
        break;
     
      default:
        // normal item
        $normal_item = views_embed_view('commerce_line_item_table', 'default', $line_item_wrapper->getIdentifier());
        if($normal_item) {
            $rows[] = array(
                'data' => array(
                    array('data' => $normal_item),
                )
            );
        }
        break;
    }
  }
  /*
  $data = $wrapper->commerce_order_total->data->value();
  if (!empty($data['components'])) {
    
    foreach ($data['components'] as $key => &$component) {
      
      if ($data['components'][$key]['name'] == 'base_price') {
        $rows[] = array(
             'data' => array(
        array('data' => '', 'style' => array('width:20%;font-weight: bold; text-align: right;')), 
        array('data' => '', 'style' => array('width:20%;font-weight: bold; text-align: right;')),     
            array('data' => '', 'style' => array('width:20%;font-weight: bold; text-align: right;')),
            array('data' => t('NET Price :'), 'style' => array('width:20%;font-weight: bold; text-align: right;')),
            array('data' => commerce_currency_format($data['components'][$key]['price']['amount'], $currency_code), 'style' => array('width:20%;font-weight: bold; text-align: right;')),
        )
       );
      }
      elseif (preg_match('/^tax\|/', $data['components'][$key]['name'])) {
        $rows[] = array(
             'data' => array(
        array('data' => '', 'style' => array('width:20%;font-weight: bold; text-align: right;')), 
        array('data' => '', 'style' => array('width:20%;font-weight: bold; text-align: right;')),      
            array('data' => '', 'style' => array('width:20%;font-weight: bold; text-align: right;')),
            array('data' => $data['components'][$key]['price']['data']['tax_rate']['display_title'] . ' :', 'style' => array('width:20%;font-weight: bold; text-align: right;')),
            array('data' => commerce_currency_format($data['components'][$key]['price']['amount'], $currency_code), 'style' => array('width:20%;font-weight: bold; text-align: right;')),
       )
                 ); 
      }
    }
  }
  
  $rows[] = array(
       'data' => array(
      array('data' => '', 'style' => array('width:20%;font-weight: bold; text-align: right;')), 
      array('data' => '', 'style' => array('width:20%;font-weight: bold; text-align: right;')),      
      array('data' => '', 'style' => array('width:20%;font-weight: bold; text-align: right;')),     
      array('data' => t('Total :'), 'style' => array('width:20%;font-weight: bold; text-align: right;')),
      array('data' => commerce_currency_format($amount*100,$currency_code), 'style' => array('width:20%;font-weight: bold; text-align: left;')),
        )
      );
  */
  
  
    // order summary (vat, discounts etc)
        $view = views_get_view('commerce_cart_summary');
        // $view = views_get_view('shopping_cart_summary_only_total');
        $view->set_arguments(array($wrapper->getIdentifier()));
        $view->hide_admin_links = TRUE;
        
        
        // $footer =  $view->get_item('default', 'footer', 'order_total');
        // $test = $view->display_handler->get_option('footer');
        $footer=   $view->preview();
        preg_match_all('/\<div class="view-footer"\>(.+?)\<\/div\>/s', $footer, $output_array);
        $order_total = $output_array[0][0];
      
        $rows[] = array(
            'data' => array(
               array('data' => '', 'style' => array('width:20%;')),
               array('data' => '', 'style' => array('width:20%;')),
               array('data' => '', 'style' => array('width:20%;')),
            // array('data' => views_embed_view('commerce_line_item_table', 'default', $line_item_wrapper->getIdentifier())),
                array('data' => $order_total, 'colspan' => 2  ), 
            )
        );
  
  return array('header' => $header, 'rows' => $rows, 'attributes' => array('style' => array('width: 100%; border: 0px solid #ddd;','sticky' => FALSE, )));
}