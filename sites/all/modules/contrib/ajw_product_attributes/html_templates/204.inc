<body>
<div id="left-panel" style="padding-top:212px;max-height: 1737px; height: 1737px; width: 50%; float: left;">
    <div id="left-panel-image">204
        <? if (isset($horizontal_left_panel_image) && $horizontal_left_panel_image != '') { ?>
            <div style="width:1472px; height:826px; margin-left:auto;margin-right:auto;text-align:center;">
                <?php echo $horizontal_left_panel_image ?>
            </div>
        <? } ?>
    </div>
</div>
<div id="right-panel" style="padding-top:100px;max-height: 1737px;height:1737px;width: 50%; float: left;">
    <div id="greeting" style="height:874px;width: 2240px; margin-left: auto; margin-right: auto; background:white;">
        <?php echo $greeting ?>
    </div>
    <div id="bottom-wrapper" style="width:2474px;margin-left:auto;margin-right:auto;text-align:center;float:left;padding-top:100px;">
        <div style="padding-left:81px;float:left;width:30%;height:670px;padding-top:340px">
            <? if ($left_bottom != '') { ?>
                <div id="left-image" style="height:297px;margin-bottom:31px;width:586px;">
                    <?php echo $left_bottom ?></div>
            <? } ?>
        </div>
        <div id="address" style="height:670px;margin-bottom:31px;width:30%;float:left;">
            <?php echo $address ?>
        </div>
        <? if ($right_bottom != '') { ?>
        <div style="float:left;width:586px;height:670px;padding-top:340px">
            <div id="right-image" style="height:293px; margin-right:81px; width:586px;">
                <?php echo $right_bottom ?></div>
        </div>
        <? } ?>     
    </div>
</div>
</body>
