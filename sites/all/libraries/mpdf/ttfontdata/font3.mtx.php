<?php
$name='Lato-Regular';
$type='TTF';
$desc=array (
  'Ascent' => 987,
  'Descent' => -213,
  'CapHeight' => 717,
  'Flags' => 4,
  'FontBBox' => '[-94 -183 1117 919]',
  'ItalicAngle' => 0,
  'StemV' => 87,
  'MissingWidth' => 532,
);
$up=-70;
$ut=60;
$ttffile='/usr/local/apache/virtualhosts/drupal7_generic/drupal7_accdev/sites/all/libraries/mpdf/ttfonts/Lato.ttf';
$TTCfontID='0';
$originalsize=120196;
$sip=false;
$smp=false;
$BMPselected=false;
$fontkey='font3';
$panose=' 8 3 2 f 5 2 2 2 4 3 2 3';
$haskerninfo=false;
$unAGlyphs=false;
?>